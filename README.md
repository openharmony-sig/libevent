# Libevent

> libevent主要是用C语言实现了事件通知的功能

![](screenshot/libevent.gif)



## 下载安装
直接在OpenHarmony-SIG仓中搜索libevent并下载。

### 使用说明

以OpenHarmony 3.1 Beta的rk3568版本为例

1. 库代码存放路径：./third_party/libevent

2. 修改添加依赖的编译脚本

   在/developtools/bytrace_standard/ohos.build文件中添加修改 "//third_party/libevent:libevent_targets","//third_party/libevent:libevent_test"

   ```
   {
     "subsystem": "developtools",
     "parts": {
       "bytrace_standard": {
         "module_list": [
           "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
           "//developtools/bytrace_standard/bin:bytrace_target",
           "//developtools/bytrace_standard/bin:bytrace.cfg",
           "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
           "//third_party/libevent:libevent_targets",
           "//third_party/libevent:libevent_test"
         ],
         "inner_kits": [
           {
             "type": "so",
             "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
             "header": {
               "header_files": [
                 "bytrace.h"
               ],
               "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
             }
           }
         ],
          "test_list": [
           "//developtools/bytrace_standard/bin/test:unittest"
         ]
       }
     }
   }
   ```

   

3. 用命令 ./build.sh --product-name rk3568 --ccache 编译

4. 生成库文件路径：

   out/rk3568/developtools/profiler（该路径生成库文件）

   out/rk3568/common/common （该路径生成生成可执行测试文件）

   

## 接口说明

1. libevent的一次性初始化
   `event_init()`或`event_base_new()`
   
2. 启用通知
   

声明一个事件结构并调用`event_set()`来初始化该结构的成员

   `event_add()`将该结构添加到受监控事件列表中，只要事件结构处于活动状态，它就必须保持分配状态。

3. 循环和分派事件

   `event_dispatch()`

4. I/O 缓冲器

   该库在常规事件回调之上提供了一个缓冲事件。提供了自动填充和排出的输入和输出缓冲区。缓冲事件的用户不再直接处理 I/O，而是从输入缓冲区读取并写入输出缓冲区

   `bufferevent_new()` 初始化

   `bufferevent_read()` 读取bufferevent结构数据

   `bufferevent_write()`  往bufferevent结构写入数据

5. 创建在一定时间到期后调用回调的计时器

   `evtimer_set ()` 准备一个事件结构以用作计时器

   `evtimer_add()` 激活计时器

   `evtimer_del()` 停用计时器

6. 异步DNS解析（使用回调来避免在执行查找时阻塞）
   `evdns_init()` 初始化

   `evdns_resolve_ipv4()` 将主机名转换为 IP 地址

   `evdns_resolve_reverse()` 执行反向查找

7. 事件驱动的 HTTP 服务器

   该库提供了一个非常简单的事件驱动 HTTP 服务器，可以嵌入到程序中并用于服务 HTTP 请求

   `evhttp_new()` 创建服务器

   `evhttp_bind_socket()` 添加要监听的地址和端口

   `evhttp_set_cb() `为每个 URI 分配一个回调

   `evhttp_set_gencb() `注册通用回调函数

## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- libevent 
|     |---- cmake   #存放cmake文件，配置cmake编译需要的头文件和源文件路径
|     |---- compat   #定义了很多宏定义，主要用于singly-linked list,list,simple queue,tail queue,circle queue等结构体
|     |---- include   #头文件
|     |---- m4   #里面存放m4文件，m4是unix下的一种编程语言，m4文件就是用m4语言写的，通常是因为configure会依赖它
|     |---- sample   #demo
|     |---- test   #单元测试的代码目录
|     |---- event.c   # event主要方法实现数
|     |---- epoll.c   #对epoll的封装
|     |---- select.c   #对select的封装
|     |---- devpull.c   #对dev/poll的封装
|     |---- kqueue.c   #对kueue的封装
|     |---- signal.c    #对信号事件的处理
|     |---- evutil.c   #些辅助功能函数的实现，包含建立socket pair和一些时间操做函数
|     |---- log.c   #log日志实现
|     |---- buffer*.c   #对缓冲区封装
|     |---- http.c   #HTTP 服务器
|     |---- evdns.c   #异步dns查询库
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/libevent/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/libevent/pulls) 。

## 开源协议
本项目基于 [BSD License](https://gitee.com/openharmony-sig/libevent/blob/master/LICENSE) ，请自由地享受和参与开源。